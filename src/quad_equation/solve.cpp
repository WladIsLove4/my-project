#include "quad_equation/solve.hpp"
#include <optional>

using namespace std;

optional<pair<double, double>> solve(double a, double b, double c, double e) {
  if (abs(a) < e) {
    return nullopt;
  }
  double d = b * b - 4.0 * a * c;

  if (d < -e) /*  d < 0  */ {
    return nullopt;
  } else if (abs(d) < e) /* d == 0 */ {
    pair<double, double> result = {};
    result.first = result.second = -b / (2.0 * a);
    return make_optional(result);
  } else /*  d > 0  */ {
    pair<double, double> result = {0.0, 0.0};
    result.first = (-b + sqrt(d)) / (2.0 * a);
    result.second = (-b - sqrt(d)) / (2.0 * a);
    return make_optional(result);
  }
}
