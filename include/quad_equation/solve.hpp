#pragma once

#include <cmath>
#include <limits>
#include <optional>
#include <utility>


std::optional<std::pair<double, double>> solve(double a, double b, double c, double e = std::numeric_limits<double>::epsilon());