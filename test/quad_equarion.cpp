#include <gtest/gtest.h>

#include <cmath>
#include <optional>

#include "quad_equation/solve.hpp"

using namespace std;

// ---------------- Helper functions -----------------------------

const double eps = std::numeric_limits<double>::epsilon();

bool compare(double left, double right, double e = eps) {
  double result = abs(left - right);
  return result < e;
}

// returns true if result contains x1 and x2 in any order
bool expected_two(optional<pair<double, double>> &input, double x1, double x2) {
  if (input == nullopt)
    return false;
  else {
    auto result = input.value();
    return (compare(result.first, x1) && compare(result.second, x2)) ||
          (compare(result.first, x2) && compare(result.second, x1));
  }
}

// returns true if result contains x and x
bool expected_one(optional<pair<double, double>> &input, double x) {

  if (input == nullopt)
    return false;
  else {
    auto result = input.value();
    return (compare(result.first, x) && compare(result.second, x));
  }
}

// returns true if there is nothing in result
bool expected_zero(optional<pair<double, double>> &input) { 
  return (input == std::nullopt); 
  }

// ---------------- Tests ----------------------------------------

TEST(QuadraticEquationTest, 1_negative2_1) {
  auto result = solve(1, -2, 1);

  EXPECT_TRUE(expected_one(result, 1.0));
}

TEST(QuadraticEquationTest, 1_0_negative1) {
  auto result = solve(1, 0, -1);

  EXPECT_TRUE(expected_two(result, -1.0, 1.0));
}

TEST(QuadraticEquationTest, 1_0_1) {
  auto result = solve(1, 0, 1);

  EXPECT_TRUE(expected_zero(result));
}

TEST(QuadraticEquationTest, a_non_zer) {
  auto result = solve(0.0, 123141, 1293647123);

  EXPECT_TRUE(expected_zero(result));
}
